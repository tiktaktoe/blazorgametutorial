#pragma checksum "D:\Blazor\Tutorial\BlazorGame\Shared\MainLayout.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "c739586e7b48f8ca30d5896cdeeb698e358dfd73"
// <auto-generated/>
#pragma warning disable 1591
namespace BlazorGame.Shared
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "D:\Blazor\Tutorial\BlazorGame\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Blazor\Tutorial\BlazorGame\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\Blazor\Tutorial\BlazorGame\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\Blazor\Tutorial\BlazorGame\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "D:\Blazor\Tutorial\BlazorGame\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "D:\Blazor\Tutorial\BlazorGame\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "D:\Blazor\Tutorial\BlazorGame\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "D:\Blazor\Tutorial\BlazorGame\_Imports.razor"
using BlazorGame;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "D:\Blazor\Tutorial\BlazorGame\_Imports.razor"
using BlazorGame.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "D:\Blazor\Tutorial\BlazorGame\_Imports.razor"
using Blazor.Extensions.Canvas;

#line default
#line hidden
#nullable disable
    public partial class MainLayout : LayoutComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddContent(0, 
#nullable restore
#line 4 "D:\Blazor\Tutorial\BlazorGame\Shared\MainLayout.razor"
         Body

#line default
#line hidden
#nullable disable
            );
        }
        #pragma warning restore 1998
    }
}
#pragma warning restore 1591
